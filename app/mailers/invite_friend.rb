class InviteFriend < ActionMailer::Base
  default from: "no-reply@todolist.com"
  default content_type: "text/html"

  def mail_to_invite_friend(email,sender)
  	@sender = sender
  	@email = email
    mail(:to => email, :subject => "Todo Invite Friend")
  end

end
