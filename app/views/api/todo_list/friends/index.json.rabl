object @users
node do|user|
	{ 
	  :id          => user.id,
	  :firstname   => user.firstname,
	  :email       => user.email,
	  :perfect_day => user.perfect_day,
	  :status      => user.request_status(@current_user.id)
	}
end