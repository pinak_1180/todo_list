json.data do
	if @data.empty?
		json.message "No Popular Task to view!"
	else		
		json.tasks @data.keys.each do |key|
			json.task_name key
			json.task_type @data[key].last.task_type
			json.count @data[key].length
			json.friends @data[key].each do |friend|
				json.id friend.user_id
				json.name friend.name
				json.perfect_day friend.perfect_day
				json.email friend.email
			end
		end
	end	
end