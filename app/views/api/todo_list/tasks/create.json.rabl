object @task
node :data do|task|
	{ :id                   => task.id,
		:weekly_point 	      => task.user.weekly_point,
    :total_points         => task.user.user_total_points,
		:point_to_stack       => task.user.point_to_stack,
		:perfect_day          => task.user.perfect_day,
		:total_task_count     => task.user.total_task_count,
		:completed_task_count => task.user.completed_task_count
	}
end

