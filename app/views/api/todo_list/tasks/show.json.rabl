object @task
attributes :task_name, :status, :point, :id, :task_type, :favorite, :inner_task

node :category do|task|
	 task.category
end

node :amount do |task|
	task.new_amount
end

node :tnumber do |task|
	task.new_tnumber
end

node :note do |task|
	task.new_note
end

node :created_at do |task|
	task.created_at.strftime("%m/%d/%Y")
end

node :data do|task|
  {
    :weekly_point          => task.user.weekly_point,
    :total_points          => task.user.user_total_points,
    :point_stack           => task.user.point_to_stack,
    :perfect_day           => task.user.perfect_day,
    :total_task_count      => task.user.total_task_count,
    :completed_task_count  => task.user.completed_task_count
  }
end