class Friend < ActiveRecord::Base

	##validations
	validates :user_one,:user_two, :presence => true
	validates_uniqueness_of :user_one, scope: :user_two, :message => "can not sent request again"
	validate :check_sender

	##callback
	before_create :add_default_status,:request_sent_notification

	##Relationship
	belongs_to :sender, class_name: 'User', :foreign_key => "user_one"
	belongs_to :receiver, class_name: 'User', :foreign_key => "user_two"

	##scopes
	scope :with_status,lambda { |status| where(status: status) }

	## Instance Methods ##
	def display_errors
	  self.errors.full_messages.join(', ')
  end

  private
  def add_default_status
  	puts "in callback add_default_status"
		self.status = 'pending'
	end

  def check_sender
		if self.user_one == self.user_two
			errors.add(:user_one, 'can not sent request.')
		end
	end

	def request_sent_notification
		puts "in callback request_sent_notification"
		sender= self.sender
		msg = "#{sender.firstname.present? ? 'sender.firstname' : 'sender.email'} has send you friend request."
		self.receiver.send_notification(msg)
	end
end
