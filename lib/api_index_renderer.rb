class ApiIndexRenderer
  attr_reader :name, :link_ref, :method_type
  
  def initialize(name, link_ref, method_type)
    @name        = name
    @link_ref    = link_ref
    @method_type = method_type
  end
  
  class << self
    def passenger_api_index
      index_arr = []
      index_arr << ApiIndexRenderer.new("Signup", "signup", "POST")
      index_arr << ApiIndexRenderer.new("Login", "login", "POST")
      index_arr << ApiIndexRenderer.new("Change password", "change_password", "POST")
      index_arr << ApiIndexRenderer.new("Forgot password", "forgot_password", "POST")
      index_arr << ApiIndexRenderer.new("Logout", "logout", "GET")
      index_arr << ApiIndexRenderer.new("Edit user", "edit_user", "POST")
      index_arr << ApiIndexRenderer.new("Get profile of user", "get_profile_of_user", "GET")
      index_arr << ApiIndexRenderer.new("Add new task", "add_new_task", "POST")
      index_arr << ApiIndexRenderer.new("Edit task", "edit_task", "POST")
      index_arr << ApiIndexRenderer.new("Completed tasks", "completed_tasks", "POST")
      index_arr << ApiIndexRenderer.new("Notify tasks", "notify_tasks", "POST")
      index_arr << ApiIndexRenderer.new("Detele task", "delete_task", "POST")
      index_arr << ApiIndexRenderer.new("Get all task", "get_all_tasks", "GET")
      index_arr << ApiIndexRenderer.new("Popular tasks", "popular_tasks", "GET")
      index_arr << ApiIndexRenderer.new("Recent tasks", "recent_tasks", "GET")
      index_arr << ApiIndexRenderer.new("Accepted or reject task", "accepte_or_reject_task", "POST")
      index_arr << ApiIndexRenderer.new("Send friend request", "send_friend_request", "POST")
      index_arr << ApiIndexRenderer.new("Status change of friend request", "status_change_of_friend_request", "POST")
      index_arr << ApiIndexRenderer.new("Delete friend request", "delete_friend_request", "GET")
      index_arr << ApiIndexRenderer.new("List friends, registered and unregistered users", "contact_list", "POST")
      index_arr << ApiIndexRenderer.new("List friends with Perfect day", "list_friends_with_perfect_day", "GET")
      index_arr << ApiIndexRenderer.new("Search User by contact number or name or email", "search", "GET")
      index_arr << ApiIndexRenderer.new("Popular Task of friends", "friend_popular_task", "GET")
      index_arr << ApiIndexRenderer.new("Invite friend", "invite_friend", "POST")
      index_arr << ApiIndexRenderer.new("Visit friend", "visit_friend", "GET")
      index_arr
    end    
  end
end