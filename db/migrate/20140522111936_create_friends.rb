class CreateFriends < ActiveRecord::Migration
  def change
    create_table :friends do |t|
      t.integer :user_one
      t.integer :user_two
      t.string :status

      t.timestamps
    end
  end
end
