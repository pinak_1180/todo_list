class ChangePhoneNumberType < ActiveRecord::Migration
  def self.up
  	rename_column :users, :contact_number, :old_contact_number
  	add_column :users, :contact_number, :bigint
  	User.all.each do |user|
  		user.contact_number = user.old_contact_number
  		user.save
  	end
  	remove_column :users, :old_contact_number
  end

  def self.down
  	rename_column :users, :contact_number, :old_contact_number
  	add_column :users, :contact_number, :integer
  	User.all.each do |user|
  		user.contact_number = user.old_contact_number
  		user.save
  	end
  	remove_column :users, :old_contact_number
  end
end
