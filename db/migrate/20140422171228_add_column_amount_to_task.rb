class AddColumnAmountToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :amount, 	 :integer, :default => 0
    add_column :tasks, :tnumber,   :integer, :default => 0
    add_column :tasks, :note,			 :text
    add_column :tasks, :category,  :string
    add_column :tasks, :task_type, :string
  end
end
